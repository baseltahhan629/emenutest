﻿using Emenu.Models.Main;
using Microsoft.EntityFrameworkCore;

namespace Emenu.SqlServer
{
    public class EmenuDbContext : DbContext
    {
        #region Properties and constructors

        public EmenuDbContext(DbContextOptions options) : base(options)
        {
        }
        #endregion

        #region DbSets

        public DbSet<CategorySet> Categories { get; set; }
        public DbSet<FileSet> Files { get; set; }
        public DbSet<ProductCategorySet> ProductsCategories { get; set; }
        public DbSet<ProductSet> Products { get; set; }
        public DbSet<ProductTranslationSet> ProductsTranslations { get; set; }
        #endregion

        #region Methods

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
        #endregion
    }
}
