﻿using System.Linq.Expressions;

namespace Emenu.Repository.IData.Base
{
    public interface IGenericRepo<T> where T : class
    {
        Task<T> GetByIdAsync(int id);
        T GetById(int id);
        void Add(T entity);
        Task AddAsync(T entity);
        Task AddRangeAsync(IEnumerable<T> entities);
        void ForceDelete(T entity);
        void Update(T entity);
        void UpdateRange(IEnumerable<T> entities);
        IQueryable<T> Find(Expression<Func<T, bool>> predicate = null, List<string> includes = null);
    }
}
