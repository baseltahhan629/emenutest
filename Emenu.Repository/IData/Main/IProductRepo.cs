﻿using Emenu.Repository.Dto.Main.Product;
using Emenu.Repository.Dto.Main.Translation;

namespace Emenu.Repository.IData.Main
{
    public interface IProductRepo
    {
        Task<int> AddProductAsync(ProductDto dto);
        Task UpdateProductAsync(int id, ProductDto dto);
        Task LinkProductFilesAsync(int id, List<string> urls);
        (IEnumerable<ProductTableDto>, int) GetProductsTable(ProductFilterDto filterDto);
        ProductInfoDto GetProductInfo(int id);
        IEnumerable<TranslationDto> GetProductTranslations(int id);
        void Delete(int id);
        bool Exist(int id);
    }
}
