﻿using Emenu.Repository.Dto.Main.Category;

namespace Emenu.Repository.IData.Main
{
    public interface ICategoryRepo
    {
        Task<int> AddCategoryAsync(CategoryDto dto);
        Task UpdateCategoryAsync(int id, CategoryDto dto);
        (IEnumerable<CategoryInfoDto>, int) GetCategoriesTable(CategoryFilterDto filterDto);
        CategoryInfoDto GetCategoryInfo(int id);
        void Delete(int id);
        bool Exist(int id);
    }
}
