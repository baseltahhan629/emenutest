﻿using Emenu.Models.Main;
using Emenu.Repository.Data.Base;
using Emenu.Repository.Dto.Main.Category;
using Emenu.Repository.Dto.Main.Product;
using Emenu.Repository.Dto.Main.Translation;
using Emenu.Repository.IData.Main;
using Emenu.Shared.Helpers;
using Emenu.SqlServer;

namespace Emenu.Repository.Data.Main
{
    public class ProductRepo : GenericRepo<ProductSet>, IProductRepo
    {
        #region Properties and constructors

        public ProductRepo(EmenuDbContext context) : base(context)
        {
        }
        #endregion

        #region Add

        public async Task<int> AddProductAsync(ProductDto dto)
        {
            var entity = new ProductSet
            {
                Cost = dto.Cost,
                Description = dto.Description,
                InventoryNumber = dto.InventoryNumber,
                Price = dto.Price,
                Translations = dto.Translations.Select(e => new ProductTranslationSet
                {
                    Culture = e.Culture,
                    Name = e.Name
                }).ToList()
            };
            await AddAsync(entity);
            return entity.Id;
        }
        #endregion

        #region Update

        public async Task UpdateProductAsync(int id, ProductDto dto)
        {
            var entity = await GetByIdAsync(id);
            entity.Cost = dto.Cost;
            entity.Description = dto.Description;
            entity.InventoryNumber = dto.InventoryNumber;
            entity.Price = dto.Price;
            entity.Translations = dto.Translations.Select(e => new ProductTranslationSet
            {
                Culture = e.Culture,
                Name = e.Name
            }).ToList();
            Update(entity);
        }

        public async Task LinkProductFilesAsync(int id, List<string> urls)
        {
            var entity = await GetByIdAsync(id);
            await Context.Entry(entity).Collection(e => e.Files).LoadAsync();
            entity.Files = urls.Select(e => new FileSet
            {
                Url = e
            }).ToList();
            Update(entity);
        }
        #endregion

        #region Get

        public ProductInfoDto GetProductInfo(int id) =>
            Find(E => !E.IsDeleted && E.Id == id)
            .Select(e => new ProductInfoDto
            {
                Cost = e.Cost,
                Description = e.Description,
                InventoryNumber = e.InventoryNumber,
                Price = e.Price,
                Categories = e.Categories.Select(e => new CategoryInfoDto
                {
                    Id = e.CategoryId,
                    Name = e.Category.Name
                }).ToList(),
                Files = e.Files.Select(e => e.Url).ToList(),
                Translations = e.Translations.Select(e => new TranslationDto
                {
                    Culture = e.Culture,
                    Name = e.Name
                }).ToList()
            }).FirstOrDefault();

        public (IEnumerable<ProductTableDto>, int) GetProductsTable(ProductFilterDto filterDto)
        {
            var data = Find(e => !e.IsDeleted);
            data = GetFilteredData(data, filterDto);
            data = string.IsNullOrEmpty(filterDto.Query) ? data : GetDeepSearch(data, filterDto.Query);
            data = filterDto.Sort == null ? data : GetSortedData(data, filterDto.Sort);
            var total = data.Count();
            data = GetPaginated(data, new Pagination
            {
                PageNumber = filterDto.PageNumber,
                PageSize = filterDto.PageSize
            });
            return (data.Select(e => new ProductTableDto
            {
                Id = e.Id,
                Cost = e.Cost,
                Description = e.Description,
                InventoryNumber = e.InventoryNumber,
                Price = e.Price
            }).ToList(), total);
        }

        public IEnumerable<TranslationDto> GetProductTranslations(int id) =>
            Context.ProductsTranslations.Where(e => !e.IsDeleted && e.ProductId == id)
            .Select(e => new TranslationDto
            {
                Culture = e.Culture,
                Name = e.Name
            }).ToList();
        #endregion

        #region Delete

        public void Delete(int id)
        {
            var entity = GetById(id);
            entity.IsDeleted = true;
            Context.Entry(entity).Collection(e => e.Translations).Load();
            Context.Entry(entity).Collection(e => e.Files).Load();
            Context.Entry(entity).Collection(e => e.Categories).Load();
            foreach (var item in entity.Translations)
            {
                item.IsDeleted = true;
            }

            foreach (var item in entity.Files)
            {
                item.IsDeleted = true;
            }

            foreach (var item in entity.Categories)
            {
                item.IsDeleted = true;
            }
            Update(entity);
        }
        #endregion

        #region Helpers

        public bool Exist(int id) =>
            Find(e => !e.IsDeleted && e.Id == id).Any();
        #endregion

        #region Helper methods

        private IQueryable<ProductSet> GetFilteredData(IQueryable<ProductSet> data
            , ProductFilterDto filterDto) =>
            data.Where(e =>
              (
               !filterDto.Price.HasValue
               || e.Price == filterDto.Price
              )
              &&
              (
                 !filterDto.Cost.HasValue
                 || e.Cost == filterDto.Cost
              )

              &&
              (
               string.IsNullOrEmpty(filterDto.InventoryNumber)
               || e.InventoryNumber.ToLower().Contains(filterDto.InventoryNumber.ToLower())
              )

              &&
              (
               string.IsNullOrEmpty(filterDto.Description)
               || e.Description.ToLower().Contains(filterDto.Description.ToLower())
              )
            );

        private IQueryable<ProductSet> GetDeepSearch(IQueryable<ProductSet> data
            , string query)
        {
            if (!string.IsNullOrEmpty(query))
                query = query.ToLower();
            data = data.Where(e =>
                e.Price.ToString().Contains(query)

                || e.Cost.ToString().Contains(query)

                || e.InventoryNumber.ToLower().Contains(query)

                || e.Description.ToLower().Contains(query)
            );
            return data;
        }

        private IQueryable<ProductSet> GetSortedData(IQueryable<ProductSet> data
         , ProductSortDto sort)
        {
            if (!string.IsNullOrEmpty(sort.Description))
            {
                data = sort.Description == Constants.ASC
                     ? data.OrderBy(e => e.Description)
                     : data.OrderByDescending(e => e.Description);
            }

            if (!string.IsNullOrEmpty(sort.InventoryNumber))
            {
                data = sort.InventoryNumber == Constants.ASC
                     ? data.OrderBy(e => e.InventoryNumber)
                     : data.OrderByDescending(e => e.InventoryNumber);
            }

            if (!string.IsNullOrEmpty(sort.Price))
            {
                data = sort.Price == Constants.ASC
                     ? data.OrderBy(e => e.Price)
                     : data.OrderByDescending(e => e.Price);
            }

            if (!string.IsNullOrEmpty(sort.Cost))
            {
                data = sort.Cost == Constants.ASC
                     ? data.OrderBy(e => e.Cost)
                     : data.OrderByDescending(e => e.Cost);
            }

            return data;
        }
        #endregion
    }
}
