﻿using Emenu.Models.Main;
using Emenu.Repository.Data.Base;
using Emenu.Repository.Dto.Main.Category;
using Emenu.Repository.IData.Main;
using Emenu.Shared.Helpers;
using Emenu.SqlServer;

namespace Emenu.Repository.Data.Main
{
    public class CategoryRepo : GenericRepo<CategorySet>, ICategoryRepo
    {
        #region Properties and constructors

        public CategoryRepo(EmenuDbContext context) : base(context)
        {
        }
        #endregion

        #region Add

        public async Task<int> AddCategoryAsync(CategoryDto dto)
        {
            var entity = new CategorySet
            {
                Name = dto.Name
            };
            await AddAsync(entity);
            return entity.Id;
        }
        #endregion

        #region Update

        public async Task UpdateCategoryAsync(int id, CategoryDto dto)
        {
            var entity = await GetByIdAsync(id);
            entity.Name = dto.Name;
            Update(entity);
        }
        #endregion

        #region Get

        public (IEnumerable<CategoryInfoDto>, int) GetCategoriesTable(CategoryFilterDto filterDto)
        {
            var data = Find(e => !e.IsDeleted);
            data = GetFilteredData(data, filterDto);
            var total = data.Count();
            data = GetPaginated(data, new Pagination
            {
                PageNumber = filterDto.PageNumber,
                PageSize = filterDto.PageSize
            });
            return (data.Select(e => new CategoryInfoDto
            {
                Id = e.Id,
                Name = e.Name
            }).ToList(), total);
        }

        public CategoryInfoDto GetCategoryInfo(int id) =>
            Find(e => !e.IsDeleted && e.Id == id)
            .Select(e => new CategoryInfoDto
            {
                Id = e.Id,
                Name = e.Name
            }).FirstOrDefault();
        #endregion

        #region Delete

        public void Delete(int id)
        {
            var entity = GetById(id);
            entity.IsDeleted = true;
            Update(entity);
        }
        #endregion

        #region Helpers

        public bool Exist(int id) =>
            Find(e => !e.IsDeleted && e.Id == id).Any();
        #endregion

        #region Helper methods

        private IQueryable<CategorySet> GetFilteredData(IQueryable<CategorySet> data,
            CategoryFilterDto filterDto) =>
            data.Where(e =>
             (string.IsNullOrEmpty(filterDto.Name)
              || e.Name.ToLower().Contains(filterDto.Name.ToLower())
             )
            &&
            (
             string.IsNullOrEmpty(filterDto.Query)
             || e.Name.ToLower().Contains(filterDto.Query.ToLower())
            )
            );

        private IQueryable<CategorySet> GetSortedData(IQueryable<CategorySet> data,
           CategorySortDto sort)
        {
            if (!string.IsNullOrEmpty(sort.Name))
            {
                data = sort.Name == Constants.ASC
                    ? data.OrderBy(e => e.Name)
                    : data.OrderByDescending(e => e.Name);
            }
            return data;
        }
        #endregion
    }
}
