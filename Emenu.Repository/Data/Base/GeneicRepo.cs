﻿using Emenu.Repository.IData.Base;
using Emenu.Shared.Helpers;
using Emenu.SqlServer;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace Emenu.Repository.Data.Base
{
    public class GenericRepo<T> : IGenericRepo<T> where T : class
    {
        protected EmenuDbContext Context;
        internal DbSet<T> dbSet;

        public GenericRepo(EmenuDbContext context)
        {
            Context = context;
            dbSet = context.Set<T>();
        }

        public async Task<T> GetByIdAsync(int id)
        {
            return await dbSet.FindAsync(id);
        }

        public T GetById(int id)
        {
            return dbSet.Find(id);
        }

        public IQueryable<T> Find(Expression<Func<T, bool>> predicate = null, List<string> includes = null)
        {
            IQueryable<T> query = dbSet;

            if (includes != null)

                foreach (var include in includes)
                {
                    query = query.Include(include);
                }
            if (predicate != null)
                return query.Where(predicate);
            else
                return query;
        }

        public IQueryable<T> GetPaginated(IQueryable<T> list, Pagination pagination)
        {
            return list.Skip(pagination.PageNumber * pagination.PageSize).Take(pagination.PageSize);
        }

        public async Task AddAsync(T entity)
        {
            await dbSet.AddAsync(entity);
        }

        public async Task AddRangeAsync(IEnumerable<T> entities)
        {
            await dbSet.AddRangeAsync(entities);
        }

        public void Add(T entity)
        {
            dbSet.Add(entity);
        }

        public void Update(T entity)
        {
            dbSet.Update(entity);
        }

        public void UpdateRange(IEnumerable<T> entities)
        {
            dbSet.UpdateRange(entities);
        }

        public void ForceDelete(T entity)
        {
            Context.Remove(entity);
        }
    }
}
