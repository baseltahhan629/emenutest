﻿using Emenu.Repository.Dto.Main.Translation;
using Microsoft.AspNetCore.Http;

namespace Emenu.Repository.Dto.Main.Product
{
    public class ProductDto
    {
        public string? Description { get; set; }
        public required string InventoryNumber { get; set; }
        public required float Price { get; set; }
        public required float Cost { get; set; }

        public List<int>? CategoriesIds { get; set; }
        public List<IFormFile>? Files { get; set; }
        public required List<TranslationDto> Translations { get; set; }
    }
}
