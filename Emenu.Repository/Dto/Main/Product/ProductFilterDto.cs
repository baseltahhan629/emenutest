﻿using Emenu.Repository.Dto.Shared;

namespace Emenu.Repository.Dto.Main.Product
{
    public class ProductFilterDto : GeneralFilterDto
    {
        public string? Description { get; set; }
        public string? InventoryNumber { get; set; }
        public float? Price { get; set; }
        public float? Cost { get; set; }
        public ProductSortDto? Sort { get; set; }
    }
}
