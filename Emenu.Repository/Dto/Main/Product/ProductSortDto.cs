﻿namespace Emenu.Repository.Dto.Main.Product
{
    public class ProductSortDto
    {
        public string? Description { get; set; }
        public string? InventoryNumber { get; set; }
        public string? Price { get; set; }
        public string? Cost { get; set; }
    }
}
