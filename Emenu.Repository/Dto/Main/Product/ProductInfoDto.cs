﻿using Emenu.Repository.Dto.Main.Category;
using Emenu.Repository.Dto.Main.Translation;

namespace Emenu.Repository.Dto.Main.Product
{
    public class ProductInfoDto
    {
        public string? Description { get; set; }
        public string InventoryNumber { get; set; }
        public float Price { get; set; }
        public float Cost { get; set; }

        public List<CategoryInfoDto> Categories { get; set; }
        public List<string> Files { get; set; }
        public List<TranslationDto> Translations { get; set; }
    }
}
