﻿using Emenu.Repository.Dto.Shared;

namespace Emenu.Repository.Dto.Main.Category
{
    public class CategoryFilterDto : GeneralFilterDto
    {
        public string? Name { get; set; }
        public CategorySortDto? Sort { get; set; }
    }
}
