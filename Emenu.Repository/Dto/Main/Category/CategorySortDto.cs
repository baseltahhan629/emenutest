﻿namespace Emenu.Repository.Dto.Main.Category
{
    public class CategorySortDto
    {
        public string? Name { get; set; } // its value will be ASC or DESC
    }
}
