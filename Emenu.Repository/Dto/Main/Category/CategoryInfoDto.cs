﻿using Emenu.Repository.Dto.Shared;

namespace Emenu.Repository.Dto.Main.Category
{
    public class CategoryInfoDto : SharedDto
    {
        public string Name { get; set; }
    }
}
