﻿namespace Emenu.Repository.Dto.Main.Category
{
    public class CategoryDto
    {
        public required string Name { get; set; }
    }
}
