﻿namespace Emenu.Repository.Dto.Main.Translation
{
    public class TranslationDto
    {
        public required string Culture { get; set; }
        public required string Name { get; set; }
    }
}
