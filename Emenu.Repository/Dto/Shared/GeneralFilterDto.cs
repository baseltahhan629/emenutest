﻿using Emenu.Shared.Helpers;

namespace Emenu.Repository.Dto.Shared
{
    public class GeneralFilterDto
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; } = Constants.PageSize;
        public string? Query { get; set; }
    }
}
