﻿using Emenu.Models.Shared;

namespace Emenu.Models.Main
{
    public class FileSet : BaseModel
    {
        #region Properties

        public string Url { get; set; }
        #endregion

        #region Navigation properties

        public ProductSet Product { get; set; }
        public int ProductId { get; set; }
        #endregion
    }
}
