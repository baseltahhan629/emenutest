﻿using Emenu.Models.Shared;

namespace Emenu.Models.Main
{
    public class CategorySet : BaseModel
    {
        #region Properties

        public string Name { get; set; }
        #endregion

        #region Navigation properties

        public ICollection<ProductCategorySet> Products { get; set; }
        #endregion
    }
}
