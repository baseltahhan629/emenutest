﻿using Emenu.Models.Shared;

namespace Emenu.Models.Main
{
    public class ProductSet : BaseModel
    {
        #region Properties

        public string? Description { get; set; }
        public string InventoryNumber { get; set; }
        public float Price { get; set; }
        public float Cost { get; set; }
        #endregion

        #region Navigation properties

        public ICollection<ProductCategorySet> Categories { get; set; }
        public ICollection<FileSet> Files { get; set; }
        public ICollection<ProductTranslationSet> Translations { get; set; }
        #endregion
    }
}
