﻿using Emenu.Models.Shared;

namespace Emenu.Models.Main
{
    public class ProductTranslationSet : BaseModel
    {
        #region Properties

        public string Culture { get; set; }
        public string Name { get; set; }
        #endregion

        #region Navigation properties

        public ProductSet Product { get; set; }
        public int ProductId { get; set; }
        #endregion
    }
}
