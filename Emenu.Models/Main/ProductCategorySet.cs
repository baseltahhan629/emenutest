﻿using Emenu.Models.Shared;

namespace Emenu.Models.Main
{
    public class ProductCategorySet : BaseModel
    {
        public ProductSet Product { get; set; }
        public int ProductId { get; set; }

        public CategorySet Category { get; set; }
        public int CategoryId { get; set; }
    }
}
