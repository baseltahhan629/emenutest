﻿namespace Emenu.Models.Shared
{
    public class BaseModel
    {
        public int Id { get; set; }
        public bool IsDeleted { get; set; }
    }
}
