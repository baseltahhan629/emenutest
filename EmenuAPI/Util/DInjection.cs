﻿using Emenu.Service.IService.Main;
using Emenu.Service.Service.Main;

namespace EmenuAPI.Util
{
    public static class DInjection
    {
        public static IServiceCollection InjectApplicationServices(this IServiceCollection services)
        {
            #region Main

            services.AddTransient<ICategoryService, CategoryService>();
            services.AddTransient<IProductService, ProductService>();
            #endregion

            return services;
        }
    }
}
