﻿using Emenu.Shared.Helpers;
using Microsoft.AspNetCore.Mvc;

namespace EmenuAPI.Controllers.Base
{
    [ApiController]
    [Route("api/[controller]")]
    public class SharedController : ControllerBase
    {
        [ApiExplorerSettings(IgnoreApi = true)]
        public JsonResult GetResult(APIResponse<object> response)
        {
            return new JsonResult(new { data = response.Data, message = response.Message })
            { StatusCode = response.Status };
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        public JsonResult GetTableResult(APITableResponse<object> response)
        {
            return new JsonResult(new
            {
                data = new
                {
                    Pagination = response.Pagination,
                    List = response.List
                },
                message = response.Message
            })
            { StatusCode = response.Status };
        }
    }
}
