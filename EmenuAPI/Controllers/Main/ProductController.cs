﻿using Emenu.Repository.Dto.Main.Product;
using Emenu.Service.IService.Main;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;

namespace EmenuAPI.Controllers.Main
{
    public class ProductController : SharedController
    {
        #region Properties and constructors

        private IProductService _productService { get; }

        public ProductController(IProductService productService)
        {
            _productService = productService;
        }
        #endregion

        #region Post

        [HttpPost("[action]")]
        public async Task<IActionResult> AddProduct([FromForm] ProductDto dto)
        {
            var response = await _productService.AddProductAsync(dto);
            return GetResult(response);
        }
        #endregion

        #region Put

        [HttpPut("[action]/{id}")]
        public async Task<IActionResult> UpdateProduct(int id, [FromForm] ProductDto dto)
        {
            var response = await _productService.UpdateProductAsync(id, dto);
            return GetResult(response);
        }
        #endregion

        #region Get


        [HttpGet("[action]/{id}")]
        public IActionResult GetProductInfo(int id)
        {
            var response = _productService.GetProductInfo(id);
            return GetResult(response);
        }

        [HttpGet("[action]")]
        public IActionResult GetProductsTable(string? filterDto)
        {
            var dto = filterDto.IsNullOrEmpty() ? new ProductFilterDto()
                : JsonConvert.DeserializeObject<ProductFilterDto>(filterDto);

            var response = _productService.GetProductsTable(dto);
            return GetTableResult(response);
        }

        [HttpGet("[action]/{id}")]
        public IActionResult GetProductTranslations(int id)
        {
            var response = _productService.GetProductTranslations(id);
            return GetResult(response);
        }
        #endregion

        #region Delete

        [HttpDelete("[action]/{id}")]
        public IActionResult Delete(int id)
        {
            var response = _productService.Delete(id);
            return GetResult(response);
        }
        #endregion
    }
}
