﻿using Emenu.Repository.Dto.Main.Category;
using Emenu.Service.IService.Main;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;

namespace EmenuAPI.Controllers.Main
{
    public class CategoryController : SharedController
    {
        #region properties and constructors


        private ICategoryService _categoryService { get; }

        public CategoryController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }
        #endregion

        #region Post

        [HttpPost("[action]")]
        public async Task<IActionResult> AddCategory(CategoryDto dto)
        {
            var response = await _categoryService.AddCategoryAsync(dto);
            return GetResult(response);
        }
        #endregion

        #region Put   

        [HttpPut("[action]/{id}")]
        public async Task<IActionResult> UpdateCategory(int id, CategoryDto dto)
        {
            var response = await _categoryService.UpdateCategoryAsync(id, dto);
            return GetResult(response);
        }
        #endregion

        #region Get

        [HttpGet("[action]")]
        public IActionResult GetCategoriesTable(string? filterDto)
        {
            var dto = filterDto.IsNullOrEmpty() ? new CategoryFilterDto()
                 : JsonConvert.DeserializeObject<CategoryFilterDto>(filterDto);
            var response = _categoryService.GetCategoriesTable(dto);
            return GetTableResult(response);
        }

        [HttpGet("[action]/{id}")]
        public IActionResult GetCategoryInfo(int id)
        {
            var response = _categoryService.GetCategoryInfo(id);
            return GetResult(response);
        }
        #endregion

        #region Delete

        [HttpDelete("[action]/{id}")]
        public IActionResult Delete(int id)
        {
            var response = _categoryService.Delete(id);
            return GetResult(response);
        }
        #endregion
    }
}
