using Emenu.Service.IService.Base;
using Emenu.Service.Service.Base;
using Emenu.Shared.Helpers;
using Emenu.SqlServer;
using EmenuAPI.Util;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
builder.Services.AddCors();
builder.Services.AddDbContext<EmenuDbContext>(x =>
             x.UseSqlServer(Constants.DefaultConnectionString
           , sqlDbOptions => sqlDbOptions.MigrationsAssembly(Constants.MigrationTargetProject)
            ));

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddTransient<IUnitOfWork, UnitOfWork>();

builder.Services.InjectApplicationServices();

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
