﻿using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using Emenu.Repository.Dto.Main.Product;
using Emenu.Service.IService.Base;
using Emenu.Service.IService.Main;
using Emenu.Shared.Helpers;

namespace Emenu.Service.Service.Main
{
    public class ProductService : IProductService
    {
        #region Properties and constructors

        private IUnitOfWork _unitOfWork { get; }

        public ProductService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        #endregion

        #region Add

        public async Task<APIResponse<object>> AddProductAsync(ProductDto dto)
        {
            try
            {
                var id = await _unitOfWork.ProductRepo.AddProductAsync(dto);
                var urls = new List<string>();
                if (dto.Files != null && dto.Files.Count() > 0)
                {
                    Account account = new Account(
                    Constants.Cloud,
                    Constants.APIKey,
                    Constants.APISecret);

                    Cloudinary cloudinary = new Cloudinary(account);

                    foreach (var file in dto.Files)
                    {
                        var uploadParams = new ImageUploadParams()
                        {
                            File = new FileDescription(file.FileName, file.OpenReadStream()),
                            PublicId = "Test_Image"
                        };
                        var uploadResult = cloudinary.Upload(uploadParams);
                        if (uploadResult.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            // The file was uploaded successfully
                            var imageUrl = uploadResult.SecureUrl.ToString();
                            urls.Add(imageUrl);
                            // Do something with the uploaded image URL
                        }
                        else
                        {
                            // Handle upload failure
                            var error = uploadResult.Error.Message;
                            return Responder.Fail(error);
                            // Handle the error
                        }
                    }
                    await _unitOfWork.ProductRepo.LinkProductFilesAsync(id, urls);
                }
                await _unitOfWork.CompleteAsync();
                return Responder.Success();
            }
            catch (Exception)
            {
                return Responder.Exception();
            }
        }
        #endregion

        #region Update

        public async Task<APIResponse<object>> UpdateProductAsync(int id, ProductDto dto)
        {
            try
            {
                #region validation

                var exist = _unitOfWork.ProductRepo.Exist(id);
                if (!exist)
                    return Responder.Fail(SharedMessages.NotFound);
                #endregion

                await _unitOfWork.ProductRepo.UpdateProductAsync(id, dto);
                var urls = new List<string>();
                if (dto.Files != null && dto.Files.Count() > 0)
                {
                    Account account = new Account(
                    Constants.Cloud,
                    Constants.APIKey,
                    Constants.APISecret);

                    Cloudinary cloudinary = new Cloudinary(account);

                    foreach (var file in dto.Files)
                    {
                        var uploadParams = new ImageUploadParams()
                        {
                            File = new FileDescription(file.FileName, file.OpenReadStream()),
                            PublicId = Constants.PublicId
                        };
                        var uploadResult = cloudinary.Upload(uploadParams);
                        if (uploadResult.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            // The file was uploaded successfully
                            var imageUrl = uploadResult.SecureUrl.ToString();
                            urls.Add(imageUrl);
                            // Do something with the uploaded image URL
                        }
                        else
                        {
                            // Handle upload failure
                            var error = uploadResult.Error.Message;
                            return Responder.Fail(error);
                            // Handle the error
                        }
                    }
                    await _unitOfWork.ProductRepo.LinkProductFilesAsync(id, urls);
                }
                await _unitOfWork.CompleteAsync();
                return Responder.Success();
            }
            catch (Exception)
            {
                return Responder.Exception();
            }
        }
        #endregion

        #region Get

        public APIResponse<object> GetProductInfo(int id)
        {
            try
            {
                #region validation

                var exist = _unitOfWork.ProductRepo.Exist(id);
                if (!exist)
                    return Responder.Fail(SharedMessages.NotFound);
                #endregion
                var data = _unitOfWork.ProductRepo.GetProductInfo(id);
                return Responder.Success(data);
            }
            catch (Exception)
            {
                return Responder.Exception();
            }
        }

        public APIResponse<object> GetProductTranslations(int id)
        {
            try
            {
                #region validation

                var exist = _unitOfWork.ProductRepo.Exist(id);
                if (!exist)
                    return Responder.Fail(SharedMessages.NotFound);
                #endregion
                var data = _unitOfWork.ProductRepo.GetProductTranslations(id);
                return Responder.Success(data);
            }
            catch (Exception)
            {
                return Responder.Exception();
            }
        }

        public APITableResponse<object> GetProductsTable(ProductFilterDto filterDto)
        {
            try
            {
                var data = _unitOfWork.ProductRepo.GetProductsTable(filterDto);
                return TableResponder.Success(data.Item1, new Pagination
                {
                    PageNumber = filterDto.PageNumber,
                    PageSize = filterDto.PageSize,
                    TotalCount = data.Item2
                });
            }
            catch (Exception)
            {
                return TableResponder.Exception();
            }
        }
        #endregion

        #region Delete

        public APIResponse<object> Delete(int id)
        {
            try
            {
                #region validation

                var exist = _unitOfWork.ProductRepo.Exist(id);
                if (!exist)
                    return Responder.Fail(SharedMessages.NotFound);
                #endregion

                _unitOfWork.ProductRepo.Delete(id);
                _unitOfWork.Complete();
                return Responder.Success();
            }
            catch (Exception)
            {
                return Responder.Exception();
            }
        }
        #endregion
    }
}
