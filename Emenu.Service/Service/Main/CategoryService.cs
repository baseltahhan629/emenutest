﻿using Emenu.Service.IService.Base;
using Emenu.Service.IService.Main;
using Emenu.Shared.Helpers;

namespace Emenu.Service.Service.Main
{
    public class CategoryService : ICategoryService
    {
        #region Properties and constructors

        private IUnitOfWork _unitOfWork { get; }

        public CategoryService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        #endregion

        #region Add

        public async Task<APIResponse<object>> AddCategoryAsync(CategoryDto dto)
        {
            try
            {
                var id = await _unitOfWork.CategoryRepo.AddCategoryAsync(dto);
                var data = _unitOfWork.CategoryRepo.GetCategoryInfo(id);
                await _unitOfWork.CompleteAsync();
                return Responder.Success(data);
            }
            catch (Exception)
            {
                return Responder.Exception();
            }
        }
        #endregion

        #region Update

        public async Task<APIResponse<object>> UpdateCategoryAsync(int id, CategoryDto dto)
        {
            try
            {
                #region validation

                var exist = _unitOfWork.CategoryRepo.Exist(id);
                if (!exist)
                    return Responder.Fail(SharedMessages.NotFound);
                #endregion

                await _unitOfWork.CategoryRepo.UpdateCategoryAsync(id, dto);
                await _unitOfWork.CompleteAsync();
                return Responder.Success();
            }
            catch (Exception)
            {
                return Responder.Exception();
            }
        }
        #endregion

        #region Get

        public APITableResponse<object> GetCategoriesTable(CategoryFilterDto filterDto)
        {
            try
            {
                var data = _unitOfWork.CategoryRepo.GetCategoriesTable(filterDto);
                return TableResponder.Success(data.Item1, new Pagination
                {
                    PageNumber = filterDto.PageNumber,
                    PageSize = filterDto.PageSize,
                    TotalCount = data.Item2
                });

            }
            catch (Exception)
            {
                return TableResponder.Exception();
            }
        }

        public APIResponse<object> GetCategoryInfo(int id)
        {
            try
            {
                #region validation

                var exist = _unitOfWork.CategoryRepo.Exist(id);
                if (!exist)
                    return Responder.Fail(SharedMessages.NotFound);
                #endregion

                var data = _unitOfWork.CategoryRepo.GetCategoryInfo(id);
                return Responder.Success(data);
            }
            catch (Exception)
            {
                return Responder.Exception();
            }
        }
        #endregion

        #region Delete

        public APIResponse<object> Delete(int id)
        {
            try
            {
                #region validation

                var exist = _unitOfWork.CategoryRepo.Exist(id);
                if (!exist)
                    return Responder.Fail(SharedMessages.NotFound);
                #endregion

                _unitOfWork.CategoryRepo.Delete(id);
                _unitOfWork.Complete();
                return Responder.Success();
            }
            catch (Exception)
            {
                return Responder.Exception();
            }
        }
        #endregion
    }
}
