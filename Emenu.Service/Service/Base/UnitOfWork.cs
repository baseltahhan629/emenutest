﻿using Emenu.Repository.Data.Main;
using Emenu.Repository.IData.Main;
using Emenu.Service.IService.Base;
using Emenu.SqlServer;
using Microsoft.EntityFrameworkCore.Storage;

namespace Emenu.Service.Service.Base
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        #region Properties and constructors

        public ICategoryRepo CategoryRepo { get; private set; }
        public IProductRepo ProductRepo { get; private set; }
        private readonly EmenuDbContext _context;

        public UnitOfWork(EmenuDbContext context)
        {
            _context = context;
            CategoryRepo = new CategoryRepo(_context);
            ProductRepo = new ProductRepo(_context);
        }
        #endregion

        #region Methods

        public async Task CompleteAsync()
        {
            await _context.SaveChangesAsync();
        }

        public void Complete()
        {
            _context.SaveChanges();
        }

        public async Task<IDbContextTransaction> BeginTransAsync()
        {
            return await _context.Database.BeginTransactionAsync();
        }

        public IDbContextTransaction BeginTrans()
        {
            return _context.Database.BeginTransaction();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
        #endregion
    }
}
