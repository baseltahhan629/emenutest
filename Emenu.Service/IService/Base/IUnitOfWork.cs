﻿using Emenu.Repository.IData.Main;
using Microsoft.EntityFrameworkCore.Storage;

namespace Emenu.Service.IService.Base
{
    public interface IUnitOfWork
    {
        public ICategoryRepo CategoryRepo { get; }
        public IProductRepo ProductRepo { get; }
        Task CompleteAsync();
        void Complete();
        void Dispose();
        Task<IDbContextTransaction> BeginTransAsync();
        IDbContextTransaction BeginTrans();
    }
}
