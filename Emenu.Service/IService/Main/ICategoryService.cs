﻿using Emenu.Shared.Helpers;

namespace Emenu.Service.IService.Main
{
    public interface ICategoryService
    {
        Task<APIResponse<object>> AddCategoryAsync(CategoryDto dto);
        Task<APIResponse<object>> UpdateCategoryAsync(int id, CategoryDto dto);
        APITableResponse<object> GetCategoriesTable(CategoryFilterDto filterDto);
        APIResponse<object> GetCategoryInfo(int id);
        APIResponse<object> Delete(int id);
    }
}
