﻿using Emenu.Repository.Dto.Main.Product;
using Emenu.Shared.Helpers;

namespace Emenu.Service.IService.Main
{
    public interface IProductService
    {
        Task<APIResponse<object>> AddProductAsync(ProductDto dto);
        Task<APIResponse<object>> UpdateProductAsync(int id, ProductDto dto);
        APIResponse<object> GetProductInfo(int id);
        APITableResponse<object> GetProductsTable(ProductFilterDto filterDto);
        APIResponse<object> GetProductTranslations(int id);
        APIResponse<object> Delete(int id);
    }
}
