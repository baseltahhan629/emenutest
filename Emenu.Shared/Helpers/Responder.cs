﻿using Microsoft.AspNetCore.Http;

namespace Emenu.Shared.Helpers
{
    public static class Responder
    {
        /// <summary>
        /// This will return success response with 200 status code
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="Data">Object Data</param>
        /// <returns></returns>
        public static APIResponse<object> Success(object Data = null)
        {
            return new APIResponse<object>
            {
                Status = StatusCodes.Status200OK,
                Message = "Succceded",
                Data = Data
            };
        }

        /// <summary>
        /// This will return fail response with 400 status code
        /// </summary>
        /// <param name="msg">Failed : {msg}</param>
        /// <returns>400 , Data =NUll</returns>
        public static APIResponse<object> Fail(string msg)
        {
            return new APIResponse<object>
            {
                Status = StatusCodes.Status400BadRequest,
                Message = "Failed " + msg,
                Data = null
            };
        }
        /// <summary>
        /// This will return fail response with 500 status code
        /// </summary>
        /// <param name="msg">Failed : {msg}</param>
        /// <returns>500 , Data =NUll</returns>
        public static APIResponse<object> Exception(object Data = null)
        {
            return new APIResponse<object>
            {
                Status = StatusCodes.Status500InternalServerError,
                Message = "Something went wrong",
                Data = Data
            };
        }

        public static APIResponse<object> Fail(object contracterrors)
        {
            throw new NotImplementedException();
        }
    }

    public static class TableResponder
    {
        /// <summary>
        /// This will return success response with 200 status code
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="Data">Object Data</param>
        /// <returns></returns>
        public static APITableResponse<object> Success(object Data, Pagination Pagination)
        {
            return new APITableResponse<object>
            {
                Status = StatusCodes.Status200OK,
                Message = "Succceded",
                List = Data,
                Pagination = Pagination,
            };
        }

        /// <summary>
        /// This will return fail response with 400 status code
        /// </summary>
        /// <param name="msg">Failed : {msg}</param>
        /// <returns>400 , Data =NUll</returns>
        public static APITableResponse<object> Fail(string msg)
        {
            return new APITableResponse<object>
            {
                Status = StatusCodes.Status400BadRequest,
                Message = "Failed " + msg,
                Pagination = new Pagination(),
                List = new List<object>()
            };
        }
        /// <summary>
        /// This will return fail response with 500 status code
        /// </summary>
        /// <param name="msg">Failed : {msg}</param>
        /// <returns>500 , Data =NUll</returns>
        public static APITableResponse<object> Exception(object Data = null)
        {
            return new APITableResponse<object>
            {
                Status = StatusCodes.Status500InternalServerError,
                Message = "Something went wrong",
                Pagination = new Pagination(),
                List = new List<object>()
            };
        }
    }
}
