﻿namespace Emenu.Shared.Helpers
{
    public class APIResponse<T>
    {
        public int Status { get; set; }

        public string Message { get; set; }

        public T Data { get; set; }
    }

    public class APITableResponse<T>
    {
        public int Status { get; set; }

        public string Message { get; set; }

        public object Statistics { get; set; }
        public Pagination Pagination { get; set; }
        public T List { get; set; }
    }

    public class Pagination
    {
        public int TotalCount { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
}
