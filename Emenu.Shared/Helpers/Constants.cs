﻿namespace Emenu.Shared.Helpers
{
    public class Constants
    {
        #region Connection string

        public static readonly string DefaultConnectionString = "data Source=.;Initial Catalog=Emenu_Db;Integrated Security=True ; TrustServerCertificate = true";
        #endregion

        #region Migration

        public static readonly string MigrationTargetProject = "Emenu.SqlServer";
        #endregion

        #region General

        public static readonly int PageSize = 10;
        public static readonly string ASC= "asc";
        #endregion

        #region Cloudinary

        public static readonly string Cloud = "dcptun34j";
        public static readonly string APIKey = "215947211899956";
        public static readonly string APISecret = "***************************";
        public static readonly string PublicId = "olympic_flag";
        #endregion
    }
}
